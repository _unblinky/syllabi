# CS 233G (9 Week Schedule)

## Week 1: Course overview and introduction.
- How to succeed in class.
- Download and install game development tools for your OS. (PC, MAC, LINUX, Chromebook)
- Tour of class tools.
- The video game life cycle.

### Assignments:
- Booting: 1. Installation & Setup
- Booting: 2. Top 10


## Week 2: Do we have our git together?
### Accessing our game project repo with GitHub Desktop
- How to _clone_ classroom project repositories?
- How to _commit_ & _push_ files?
- Intro to the Godot interface.
- Godot's 3D coordinate systems.
- Basic 3D transformations _(position, rotation, scale)_

### Assignments:
- Booting: 3. Hello Godot
- Booting: 4. Nav Quest 3D


## Week 3: Anatomy of Game Programming: Classes, Instances, and the Game Tree.
- What are Objects, OOD, and OOP?
- What are _variables_.
- What are _functions_.
- Function parameters, arguments, and return types.
- Serial vs. Event Signals.
- Exploring the Game Tree at runtime.

### OOP by Game Engine
- Class _(All OOP Languages)_
- Scene _(Godot)_
- Blueprint _(Unreal)_
- Prefab _(Unity)_

### 3D Scenes
- Scene Hierarchy _(a little like layers)_
- Adding Nodes: `RMB` > `Add Child Node`
- Parenting Nodes: select a node and drag it on top of parent node.
- `RMB` > `Save Branch as Scene`
- Instancing objects from the editor and from script.
- Performance boosts with explicit type casting!

### Assignments:
- Bombs Away: 1. Project Setup
- Bombs Away: 2. It's Raining Bombs


## Week 4: What is logic flow?
### Conditions: Code blocks that branch from a bool value.
  - if:
  - elif:
  - else:

### Loops
- for element in elements:
- for i in 6:
- for i in array.size():
- for i in range(10, 0, -1): # i = 10, 9, 8, 7, 6, 5, 4, 3, 2, 1

### Assignments:
- Bombs Away: 3. Explosive Explosion
- Bombs Away: 4. Level Design


## Week 5: Don’t forget the UX!
### Mouse Input
- func _input(event):
- func _unhandled_input(event):

### UI vs. UX
- Control node.
- Label node.
- Position and scale UI anchored to viewport dimensions.

### Building a Release
- Exporting and deploying our game.

### Assignments:
- Bombs Away: 5. Missile Targeting
- Bombs Away: 6. UI & Menus


## Week 6: Blender Boost
### Intro to 3D Box Modeling.
#### Object Mode
- Add Object: `Shift` + `A`
- Select: `W`
- Grab: `G`
- Rotate: `R`
- Scale: `S`

#### Edit Mode
- Vertex Select: `1`
- Edge Select: `2`
- Face Select: `3`
- Extrude: `E`
- Inset: `I`
- Loop Cut: `Ctrl` + `R`
- Knife: `K` > `Click` > `Enter`

#### UV Unwrapping
- Marking Seams.
- UV Projection.
- Unwrapping mesh.
- Texture prep.

#### Exporting
- GLTF files to Godot.

### Assignments:
- Danger X-ing: 1. Project Setup
- Danger X-ing: 2. Player Character


## Week 7: Area and Body nodes
- Collisions with:
  - Area3D
  - StaticBody3D
  - RigidBody3D
  - CharacterBody3D

### Assignments:
- Danger X-ing: 3. Roads & Rivers
- Danger X-ing: 4. Vehicles & Vessels

## Week 8: What are the 5 pillars of OOP?
- Abstraction.
- Encapsulation.
- Inheritance.
- Polymorphism.

### Assignments:
- Danger X-ing: 5. Audio
- Danger X-ing: 6. UI & Menus


## Week 9: Paid to Play
- Where to next?
- Where do we fit?
- https://gamedevmap.com/

### Assignments:
- Game Expo: Show off your favorite class project or feature.
- Postmortem: What are 5 things that went right? What are 5 things that went wrong?
