# CS 133G (11 Week Schedule)

## Week 1: Course overview and introduction.
- How to succeed in class.
- Download and install game development tools for your OS. (PC, MAC, LINUX, Chromebook)
- Tour of class tools.
- The video game life cycle.

### Assignments:
- Booting: 1. Installation & Setup
- Booting: 2. Top 10


## Week 2: Do we have our git together?
### Accessing our game project repo with GitHub Desktop
- How to _clone_ classroom project repositories?
- How to _commit_ & _push_ files?
- Intro to the Godot interface.
- Godot's 3D coordinate systems.
- Basic 3D transformations _(position, rotation, scale)_

### Assignments:
- Booting: 3. Hello Godot
- Booting: 4. Nav Quest 3D


## Week 3: Anatomy of Game Programming: Classes, Instances, and the Game Tree.
- What are Objects, OOD, and OOP?
- What are _variables_.
- What are _functions_.
- Function parameters, arguments, and return types.
- Serial vs. Event Signals.
- Exploring the Game Tree at runtime.

### OOP by Game Engine
- Class _(All OOP Languages)_
- Scene _(Godot)_
- Blueprint _(Unreal)_
- Prefab _(Unity)_

### 2D Scenes
- Scene Hierarchy _(a little like layers)_
- Adding Nodes: `RMB` > `Add Child Node`
- Parenting Nodes: select a node and drag it on top of parent node.
- `RMB` > `Save Branch as Scene`
- Instancing objects from the editor and from script.
- Performance boosts with explicit type casting!

### Assignments:
- Bubble Pop: 1. Project Setup
- Bubble Pop: 2. Tons o' Suds


## Week 4: What is logic flow?
### Common 2D Scene Tree Nodes:
- Node2D _(has a 2D position, rotation, and scale)_
- Area2D _(has the ability to handle mouse clicks / collisions)_
- Sprite2D _(2D graphics)_
- CollisionShape2D _(the hitbox)_
- Control _(UI containers & graphics)_
- Timer _(run code when the timer counts down)_

### Conditions: Code blocks that branch from a bool value.
  - if:
  - elif:
  - else:

### Loops
- for element in elements:
- for i in 6:
- for i in array.size():
- for i in range(10, 0, -1): # i = 10, 9, 8, 7, 6, 5, 4, 3, 2, 1

### Assignments:
- Bubble Pop: 3. Click & Pop()
- Bubble Pop: 4. Score + More


## Week 5: Don’t forget the UX!
### Mouse Input
- func _input(event):
- func _unhandled_input(event):

### UI vs. UX
- Control node.
- Label node.
- Position and scale UI anchored to viewport dimensions.

### Building a Release
- Exporting and deploying our game.

### Assignments:
- Space Rocks: 1. Project Setup
- Space Rocks: 2. Wrapping Rocks


## Week 6: Krita Boost
- How do 2D digital artists make assets for video games, design docs, websites, or graphic design?
- Pixel tools vs. Vector tools.

### Intro to Pixel Pushing.
- `New File` > `Custom Document`:
  - width: `32` px.
  - heigh: `32` px.
- Activate dockers with: `Settings` > `Dockers` >
  - `Toolbox`
  - `Tool Options`
  - `Layers`
  - `Advanced Color Selector`

### Navigation
- Pan: `MMB` drag.
- Zoom In: `MMW` up.
- Zoom Out: `MMW` down.
- Select all pixels: `Ctrl` + `A`
- Deselect all pixels: `Ctrl` + `Shift` + `A`

### Painting with Light
- Paint Layer _(pixel layer)_
- Brush: `B`
- Brush Erase: `E`
- Anti-Aliasing.
- RGBA color space.

#### Painting through Time
- Keyframes on the Timeline: `RMB` > `Create Blank Keyframe`
- Onion skinning.
- Rearrange for timing.

#### Exporting
- PNG files to Godot.
- What is a sprite atlas?
- `AnimatedSprite2D` node: `Animation` > `SpriteFrames`

### Assignments:
- Space Rocks: 3. SuperWrap < Bullet
- Space Rocks: 4. Ship Input


## Week 7: Area and Body nodes
- Collisions with:
  - Area3D
  - StaticBody3D
  - RigidBody3D
  - CharacterBody3D

### Assignments:
- Space Rocks: 5. Gameplay
- Space Rocks: 6. Hi UI


## Week 8: What are the 5 pillars of OOP?
- Abstraction.
- Encapsulation.
- Inheritance.
- Polymorphism.

### Assignments:
- Final Game: 1. GDD
- Final Game: 2. Implement


## Week 9: Follow the flow.
- Design.
- Build.
- Break.
- Iterate.

### Assignments:
- Final Game: 3. Test
- Final Game: 4. Iterate


## Week 10: Breathing life into our objects.
- Animations.
- Animation Tracks.
- Key Frames:
- Linear.
- Cubic.
- Nearest.
- Onion Skinning.

### Assignments:
- Final Game: 5. Re-design
- Final Game: 6. Re-implement


## Week 12: Paid to Play
- Where to next?
- Where do we fit?
- https://gamedevmap.com/

### Assignments:
- Game Expo: Show off your favorite class project or feature.
- Postmortem: What are 5 things that went right? What are 5 things that went wrong?
